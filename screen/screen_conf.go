package screen

const (
	screenWidth       = 240
	screenHeight      = 240
	countScreenWidth  = screenWidth / TileSize
	countScreenHeight = screenHeight / TileSize
)

func GetWidth() int {
	return screenWidth
}

func GetHeight() int {
	return screenHeight
}

func init() {
	//ebiten.SetFullscreen(true)
	//screenWidth, screenHeight = ebiten.ScreenSizeInFullscreen()
	println("w:", screenWidth, "h:", screenHeight)

	//screenWidth = screenWidth - screenWidth%TileSize
	//screenHeight = screenHeight - screenHeight%TileSize

	//countScreenWidth = screenWidth / TileSize
	//countScreenHeight = screenHeight / TileSize
}
