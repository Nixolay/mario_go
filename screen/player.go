package screen

import (
	"bytes"
	"github.com/hajimehoshi/ebiten"
	"image"
	"io/ioutil"
	"log"
	"sync"
	"time"
)

const (
	PlayerSize = 48
	PlayerXNum = 8 //default 25
)

type player struct {
	image  *ebiten.Image
	screen *ebiten.Image
	op     *ebiten.DrawImageOptions
	x, y   int
	walk   [2]float64
	sleep  time.Duration
	wg     *sync.Mutex
}

func (p *player) handle() {
	p.wg = new(sync.Mutex)
	p.op.GeoM.Translate(0, 172)
	p.screen.DrawImage(p.image.SubImage(image.Rect(p.x, p.y, p.x+PlayerSize, p.y+PlayerSize)).(*ebiten.Image), p.op)
	p.sleep = time.Second / 300
	for {
		select {
		case <-time.After(p.sleep):
			switch p.walk[1] {
			case 0:
				p.op.GeoM.Translate(p.walk[0], p.walk[1])
				p.screen.DrawImage(p.image.SubImage(image.Rect(p.x, p.y, p.x+PlayerSize, p.y+PlayerSize)).(*ebiten.Image), p.op)
			default:
				p.jump()
			}

		}
	}
}
func (p *player) jump() {
	p.wg.Lock()
	defer p.wg.Unlock()
	for i := 0; i < 100; i++ {
		p.op.GeoM.Translate(p.walk[0], -1)
		p.screen.DrawImage(p.image.SubImage(image.Rect(p.x, p.y, p.x+PlayerSize, p.y+PlayerSize)).(*ebiten.Image), p.op)
		time.Sleep(p.sleep)
	}
	for i := 100; i > 0; i-- {
		p.op.GeoM.Translate(p.walk[0], 1)
		p.screen.DrawImage(p.image.SubImage(image.Rect(p.x, p.y, p.x+PlayerSize, p.y+PlayerSize)).(*ebiten.Image), p.op)
		time.Sleep(p.sleep)
	}
}

func GetPlayerImage(s *ebiten.Image) *player {
	if s == nil {
		return nil
	}
	file, err := ioutil.ReadFile("sprites/adventurer_sheet.png")
	if err != nil {
		panic(err)
	}

	img, _, err := image.Decode(bytes.NewReader(file))
	if err != nil {
		log.Fatal(err)
	}
	p := &player{op: &ebiten.DrawImageOptions{}, screen: s}
	p.image, _ = ebiten.NewImageFromImage(img, ebiten.FilterDefault)

	p.x = 0

	p.y = 0

	go p.handle()
	return p
}

func (p *player) Stay() {
	p.walk[0], p.walk[1] = 0, 0
	p.op.GeoM.Translate(0, 0)
	p.screen.DrawImage(p.image.SubImage(image.Rect(p.x, p.y, p.x+PlayerSize, p.y+PlayerSize)).(*ebiten.Image), p.op)

}

func (p *player) GoForward() {
	p.walk[0], p.walk[1] = 1, 0
}

func (p *player) GoBack() {
	p.walk[0], p.walk[1] = -1, 0
}

func (p *player) Jump() {
	p.walk[0], p.walk[1] = 0, 1
}
