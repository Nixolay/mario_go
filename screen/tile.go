// example.png = 400x224/15*14
// four-seasons-tileset_16x16.png = 176x256
package screen

import (
	"bytes"
	"github.com/hajimehoshi/ebiten"
	"image"
	"io/ioutil"
	"log"
)

const (
	TileSize = 16
	TileXNum = 11 //default 25
)

func GetTileImage() *ebiten.Image {
	file, err := ioutil.ReadFile("sprites/four-seasons-tileset_16x16.png")
	if err != nil {
		panic(err)
	}

	img, _, err := image.Decode(bytes.NewReader(file))
	if err != nil {
		log.Fatal(err)
	}
	tilesImage, _ := ebiten.NewImageFromImage(img, ebiten.FilterDefault)
	return tilesImage
}
