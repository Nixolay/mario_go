package screen

func GenerateLayers() [][]int {

	layers := make([][]int, 3)
	for i := range layers {
		layers[i] = make([]int, screenHeight*screenWidth/TileSize)
	}

	for i := range layers {
		switch i {
		case 0:
			firstLayer(layers[i])
		case 1:
			drawLayer(layers[i], map[int]int{countScreenHeight - 2: 0})
		case 2:
			drawLayer(layers[i], map[int]int{
				countScreenHeight - 1: 114,
				countScreenHeight - 2: 104,
			})
		}
	}

	return layers
}

func firstLayer(layer []int) {
	rang := 0
	for i := range layer {
		switch rang {
		case 0:
			layer[i] = 99
		case 1:
			layer[i] = 110
		default:
			layer[i] = 121
		}

		if (i+1)%countScreenWidth == 0 {
			rang++
		}
	}
}

func drawLayer(layer []int, r map[int]int) {
	rang := 0
	for i := range layer {
		if c, ok := r[rang]; ok {
			layer[i] = c
		} else {
			layer[i] = 1000
		}
		if (i+1)%countScreenWidth == 0 {
			rang++
		}
	}
}
