package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"gitlab.com/Nixolay/mario_go/screen"
	"image"
	_ "image/png"
	"log"
	"os"
)

var (
	tilesImage = screen.GetTileImage()
	player     = screen.GetPlayerImage(nil)
	layers     = screen.GenerateLayers()
)

func update(s *ebiten.Image) error {
	if ebiten.IsDrawingSkipped() {
		return nil
	}
	if player == nil {
		player = screen.GetPlayerImage(s)
	}

	pressed := []ebiten.Key{}
	for k := ebiten.Key(0); k <= ebiten.KeyMax; k++ {
		if ebiten.IsKeyPressed(k) {
			pressed = append(pressed, k)
		}
		if ebiten.IsKeyPressed(ebiten.KeyLeft) {
			player.GoBack()
		}
		if ebiten.IsKeyPressed(ebiten.KeyRight) {
			player.GoForward()
		}
		if ebiten.IsKeyPressed(ebiten.KeySpace) {
			player.Jump()
		}
		if ebiten.IsKeyPressed(ebiten.KeyEscape) {
			os.Exit(1)
		}
	}

	xNum := screen.GetWidth() / screen.TileSize
	for _, l := range layers {
		for i, t := range l {
			op := &ebiten.DrawImageOptions{}
			op.GeoM.Translate(float64((i%xNum)*screen.TileSize), float64((i/xNum)*screen.TileSize))

			sx := (t % screen.TileXNum) * screen.TileSize
			sy := (t / screen.TileXNum) * screen.TileSize
			s.DrawImage(tilesImage.SubImage(image.Rect(sx, sy, sx+screen.TileSize, sy+screen.TileSize)).(*ebiten.Image), op)
		}
	}

	player.Stay()

	ebitenutil.DebugPrint(s, fmt.Sprintf("TPS: %0.2f", ebiten.CurrentTPS()))
	//ebitenutil.DebugPrint(s, fmt.Sprintf("             FPS: %0.2f", ebiten.CurrentFPS()))

	return nil
}

func main() {
	if err := ebiten.Run(update, screen.GetWidth(), screen.GetHeight(), 2, "MarioGo"); err != nil {
		log.Fatal(err)
	}
}
